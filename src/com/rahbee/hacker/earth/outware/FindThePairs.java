package com.rahbee.hacker.earth.outware;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class FindThePairs {
	
	private static int findThePairs(int a[], int arraySize, int givenDifference){
		

		int count = 0;
		// Not a better solution 
//		for (int i = 0; i < arraySize-1; i++)
//	    {       
//	        for (int j = i+1; j < arraySize; j++){
//	        	if((a[j] - a[i]) > givenDifference)
//	        		break;
//	            if (a[j] - a[i] == givenDifference ){
//	                  count++;
//	            }
//	        }
//	    }
		int i = 0, j = 1;
		while(i < arraySize-1 && j < arraySize){
			if (a[j] - a[i] == givenDifference ){
                count++;
                i++;
                j = i + 1;
			}else if((a[j] - a[i]) > givenDifference){
				i++;
				j = i + 1;
			}else if((a[j] - a[i]) < givenDifference){
				j++;
			}
		}
		
		return count;
	}
	
	private static int[] removeDuplicateElement(int a[]){
		int len = a.length;
		Set<Integer> set = new HashSet<Integer>();

		for(int i = 0; i < len; i++){
		  set.add(a[i]);
		}

		int[] newArray = new int[set.size()];
		int i = 0;
		for (int s : set) {
			newArray[i++] = s;
		}
		return newArray;
	}
	
	
	public static void main(String[] args) throws Exception{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int arraySize = Integer.parseInt(br.readLine());
		int a[] = new int[arraySize];
		
		int i = 0;
		while(i<arraySize){
			a[i] = Integer.parseInt(br.readLine());
			i++;
		}
		int givenDifference = Integer.parseInt(br.readLine());
		
		Arrays.sort(a); // No need
		
		int newArray[] = removeDuplicateElement(a);
		
		System.out.println(findThePairs(newArray, newArray.length, givenDifference));
		
		System.out.println("Done !!");
		
	}

}
