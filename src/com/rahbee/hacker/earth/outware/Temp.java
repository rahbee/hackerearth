package com.rahbee.hacker.earth.outware;

public class Temp {
	
	private static int binarySearch(int arr[], int low, int high, int x)
	{
	    if (high >= low)
	    {
	        int mid = low + (high - low)/2;
	        if (x == arr[mid])
	            return mid;
	        if (x > arr[mid])
	            return binarySearch(arr, (mid + 1), high, x);
	        else
	            return binarySearch(arr, low, (mid -1), x);
	    }
	    return -1;
	}
	
	private static int findThePairsUsingBinarySearch(int a[], int arraySize, int givenDifference){
		int count = 0;
		for (int i = 0; i < arraySize-1; i++)
	        if (binarySearch(a, i+1, arraySize-1, a[i] + givenDifference) != -1)
	            count++;
		
		return count;
	}
	
	public static void main(String[] args) {
		for(int i=1; i<= 100; i++){
			System.out.print(i + " ");
			
			if(i%3 == 0){
				System.out.print("Spicy");
			}
			if(i%5 == 0){
				System.out.print("Chicken");
			}
			if((i%3 == 0) && (i%5 == 0)){
				System.out.print("!");
			}
			System.out.println();
		}
	}
}
