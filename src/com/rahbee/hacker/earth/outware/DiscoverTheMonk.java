package com.rahbee.hacker.earth.outware;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DiscoverTheMonk {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Integer> array = new ArrayList<>();
		List<Integer> query = new ArrayList<>();
		List<String> output = new ArrayList<>();
		Scanner sc = new Scanner(System.in);

		int N = sc.nextInt();
		int Q = sc.nextInt();
		sc.nextLine();
		
		
		//Input array
		for(int i=0; i<N && N >= 1; i++){
			int val = sc.nextInt();
			int exp = (int) Math.pow(10, 9);
			if(val >= 1 && val <= exp)
				array.add(val);
		}
		
		sc.nextLine();
		
		int expQ = (int) Math.pow(10, 5);
		
		// query
		for(int i=0; i<Q && Q<=expQ; i++){
			int val = sc.nextInt();
			int exp = (int) Math.pow(10, 9);
			if(val >= 1 && val <= exp)
				query.add(val);
			
		}
		
		
		for (Integer q : query) {
			if(array.contains(q)){
				output.add("YES");
			}else{
				output.add("NO");
			}
		}
		
		output.forEach(x -> System.out.println(x));
	}

}
