package com.rahbee.hacker.earth.outware;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class MonkAndPhilosopherStone {

	public final class Command {
		public static final String HARRY = "Harry";
		public static final String REMOVE = "Remove";

		private Command() {
		}
	}

	public static void main(String[] args) {
		Stack bagMonk = new Stack();
		List<Integer> bagHarry = new ArrayList<>();
		List<String> commands = new ArrayList<>();

		// The first line will consists of one integer
		// denoting the number of gold coins in Harry's Bag.

		Scanner sc = new Scanner(System.in);

		int N = sc.nextInt();

		// Second line contains
		// N space separated integers, denoting the worth of gold coins.

		for (int i = 0; i < N; i++) {
			bagHarry.add(sc.nextInt());
		}

		// System.out.println("N : " + N);

		// for (Integer val : bagHarry) {
		// System.out.println(val + " ");
		// }

		// Third line contains 2 space separated integers
		// Q and X, denoting the number of instructions and the value of X
		// respectively.

		int Q = sc.nextInt();
		int X = sc.nextInt();

		sc.nextLine();

		// System.out.println("Q: " + Q + " X: " + X);

		// In next Q lines, each line contains one string either "Harry"
		// (without quotes) or "Remove" (without quotes).

		for (int i = 0; i < Q; i++) {
			String s = sc.nextLine();
			commands.add(s);
		}

		// commands.forEach(x -> System.out.print(x + " "));
		int it = 0;
		for (String cmd : commands) {
			if (cmd.equals(Command.HARRY)) {
				if (bagMonk.size() < X) {
					bagMonk.push(bagHarry.get(it));
					it++;
				}
			} else if (cmd.equalsIgnoreCase(Command.REMOVE)) {
				if (bagMonk.size() >= 1)
					bagMonk.pop();
			}
		}

		System.out.println(bagMonk.size());

	}

}
