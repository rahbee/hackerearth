package com.rahbee.hacker.earth.outware;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Anagrams {
	
	private static boolean anagramFinder(String word1, String word2){
		boolean flag = false;
		
		if((word1.length() != 0) && (word2.length() != 0) && (word1.length() == word2.length())){
			char[] arr1 = word1.toCharArray();
			char[] arr2 = word2.toCharArray();
			
			Arrays.sort(arr1);
			Arrays.sort(arr2);
			
			flag = Arrays.equals(arr1, arr2);
		}
		
		return flag;
		
	}
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String word1 = br.readLine();
		String word2 = br.readLine();
	    System.out.println(anagramFinder(word1, word2));
	}
}
