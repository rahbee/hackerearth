package com.rahbee.hacker.earth.outware;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class FizzBuzz {

	public static void main(String[] args) throws Exception {
		//Scanner sc = new Scanner(System.in);
		
		//int T = sc.nextInt();
		
		//Input array
		//for(int i=0; i<T; i++){
		//	int val = sc.nextInt();
		//		N.add(val);
		//}
		
		//sc.nextLine();
		List<Integer> N = new ArrayList<Integer>();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());
		// int a[] = new int[t];
		StringTokenizer st = new StringTokenizer(br.readLine());
		while(st.hasMoreTokens()){
			N.add(Integer.parseInt(st.nextToken()));
		}
		
		//System.out.println(T);
		//System.out.println(N.toString());
		
		for (Integer val : N) {
			for(int i=1; i<=val; i++){
				if((i%3==0) && (i%5==0)){
					System.out.println("FizzBuzz");
				}else if((i%3 != 0) && (i%5==0)){
					System.out.println("Buzz");
				}else if((i%3==0) && (i%5!=0)){
					System.out.println("Fizz");
				}else{
					System.out.println(i);
				}
			}
		}

	}

}
