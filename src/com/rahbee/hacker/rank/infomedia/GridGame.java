package com.rahbee.hacker.rank.infomedia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GridGame {

	static long countX(String[] steps) {
		int maxA = 0, maxB = 0;
		int arrLength = steps.length;

		String[] strArr;
		int[] arr = new int[arrLength];
		int[] brr = new int[arrLength];
		for (int i = 0; i < arrLength; i++) {
			strArr = steps[i].split(" ");
			int a = Integer.parseInt(strArr[0]);
			int b = Integer.parseInt(strArr[1]);
			maxA = (a < maxA) ? maxA : a;
			maxB = (b < maxB) ? maxB : b;

			arr[i] = a;
			brr[i] = b;
		}

		// System.out.println("maxA : " + maxA + " maxB : " + maxB);

		int[][] grid = new int[maxA][maxB];

		for (int i = 0; i < arrLength; i++) {
			for (int j = arr[i] - 1; j >= 0; j--) {
				for (int k = brr[i] - 1; k >= 0; k--) {
					++grid[j][k];
				}
			}
		}

		int findMax = 0, maxCount = 0;

		for (int j = 0; j < maxA; j++) {
			for (int k = 0; k < maxB; k++) {
				// System.out.print("[" + j + "][" + k + "] :" + grid[j][k] + " ");
				findMax = (grid[j][k] > findMax) ? grid[j][k] : findMax;
			}
			// System.out.println();
		}
		
		for (int j = 0; j < maxA; j++) {
			for (int k = 0; k < maxB; k++) {
				if(findMax == grid[j][k])
					maxCount++;
			}
		}

		return maxCount;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		String a[] = new String[n];
		long res;

		for (int i = 0; i < n; i++) {
			a[i] = br.readLine();
		}

		res = countX(a);

		System.out.println(res);
	}
}
