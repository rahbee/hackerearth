package com.rahbee.hacker.rank.infomedia;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class JumpingJack {

	static int maxStep(int n, int k) {

		int i = 0, j =1;
		while (((j * (j + 1)) / 2) <= k) {
			if (((j * (j + 1)) / 2) == k) {
				j = -1;
				break;
			}
			j++;
		}

		if (j == -1) {
			return (((n * (n + 1) / 2) - 1));
		} else {
			return (n * (n + 1) / 2);
		}

	}

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		int k = Integer.parseInt(br.readLine());
		
		int res = maxStep(n,k);
		
		System.out.println(res);

		
	}

}
